package com.example.wheeldemo;

import java.util.HashMap;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.android.view.wheel.ArrayWheelAdapter;
import com.android.view.wheel.OnWheelChangedListener;
import com.android.view.wheel.WheelView;

public class BankAddressDialog extends Dialog implements OnWheelChangedListener {

	private static final String[] LETTERS = { "A", "B", "C", "F", "G", "H",
			"J", "L", "N", "Q", "S", "T", "X", "Y", "Z" };

	private Map<String, String[]> mProvicesMap = new HashMap<String, String[]>();
	private Map<String, String[]> mCitiesMap = new HashMap<String, String[]>();

	private Context mContext;

	private WheelView mLetterWv;
	private WheelView mProviceWv;
	private WheelView mCityWv;

	private Button mOk, mCancel;

	private String mLetter, mProvice, mCity;

	private ICitySelectorListener mCitySelectorListener;

	public interface ICitySelectorListener {
		void onCitySelect(String province, String city);

		void onCitySelectCancel();
	}

	public BankAddressDialog(Context context) {
		super(context, R.style.custom_dialog_theme);
		mContext = context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_bankaddress);
		initDatas();
		initViews();
	}

	public void setCitySelectorListener(ICitySelectorListener listener) {
		mCitySelectorListener = listener;
	}

	private void initDatas() {
		for (String s : LETTERS) {
			int arrayId = mContext.getResources().getIdentifier(s, "array",
					mContext.getPackageName());
			String[] provices = mContext.getResources().getStringArray(arrayId);
			for (String provice : provices) {
				arrayId = mContext.getResources().getIdentifier(provice,
						"array", mContext.getPackageName());
				if (arrayId == 0) {
					try {
						arrayId = R.array.class.getField(provice).getInt(null);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (NoSuchFieldException e) {
						e.printStackTrace();
					}
				}
				if(arrayId != 0){
					String[] cities = mContext.getResources().getStringArray(
							arrayId);
					mCitiesMap.put(provice, cities);
				}
			}
			mProvicesMap.put(s, provices);
		}

		mLetter = LETTERS[0];
	}

	private void initViews() {

		mLetterWv = (WheelView) findViewById(R.id.wv_letter);
		mProviceWv = (WheelView) findViewById(R.id.wv_provice);
		mCityWv = (WheelView) findViewById(R.id.wv_city);

		mOk = (Button) findViewById(R.id.bt_ok);
		mOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (mCitySelectorListener != null) {
					/*
					 * String format = "%s省%s市"; if (mProvice.equals(mCity)) {
					 * format = "%s%s市"; }
					 */
					if (!TextUtils.isEmpty(mProvice)
							&& !TextUtils.isEmpty(mCity)) {
						mCitySelectorListener.onCitySelect(mProvice, mCity);
					}
				}
				dismiss();
			}
		});
		mCancel = (Button) findViewById(R.id.bt_cancel);
		mCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (mCitySelectorListener != null) {
					mCitySelectorListener.onCitySelectCancel();
				}
				dismiss();
			}
		});

		mLetterWv.setAdapter(new ArrayWheelAdapter<String>(LETTERS,
				LETTERS.length));
		mLetterWv.addChangingListener(this);
		mProviceWv.addChangingListener(this);
		mCityWv.addChangingListener(this);

		updateProvices();
		updateCities();

	}

	private void updateProvices() {
		String[] provices = mProvicesMap.get(mLetter);
		if (provices == null) {
			provices = new String[] { "" };
		}
		mProviceWv.setAdapter(new ArrayWheelAdapter<String>(provices,
				provices.length));
		mProviceWv.setCurrentItem(0);
		mProvice = provices[0];
	}

	private void updateCities() {
		String[] cities = mCitiesMap.get(mProvice);
		if (cities == null) {
			cities = new String[] { "" };
		}
		mCityWv.setAdapter(new ArrayWheelAdapter<String>(cities, cities.length));
		mCityWv.setCurrentItem(0);
		mCity = cities[0];
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		int pos = wheel.getCurrentItem();
		if (wheel == mProviceWv) {
			mProvice = mProvicesMap.get(mLetter)[pos];
			updateCities();
		} else if (wheel == mCityWv) {
			mCity = mCitiesMap.get(mProvice)[pos];
		} else {
			mLetter = LETTERS[newValue];
			updateProvices();
			updateCities();
		}
	}
}
